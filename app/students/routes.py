from app.students import bp
from app.db import mongo
from app.models.student import Student, StudentIn, StudentOut

from flask import jsonify

 
####
## Helpers
#### 

def get_last_student_id():
    last_student_id  = mongo.students_collection.find().sort([('student_id', -1)]).limit(1)
    try:
        last_student_id = last_student_id[0]['student_id']
    except:
        last_student_id = 0
    return last_student_id

####
## view functions
####   

@bp.get('/')
@bp.output(StudentOut(many=True))
def get_all_students():
    students = mongo.students_collection.find()
    student_list = []
    for student in students:
        student_dict = {
            "student_id": student['student_id'],
            "name": student['name'],
            "level": student['level']
        }
        student_list.append(student_dict)
    return jsonify(student_list)

@bp.get('/<int:student_id>')
@bp.output(StudentOut)
def get_student(student_id):
    student = mongo.students_collection.find_one({'student_id': int(student_id)})
    student_dict = {
        "student_id" : student['student_id'],
        "name" : student['name'],
        "level" : student['level']
    }
    return jsonify(student_dict)

@bp.post('/')
@bp.input(StudentIn, location='json')
@bp.output(StudentOut, status_code=201)
def create_student(json_data):
    last_student_id = int(get_last_student_id())  # Konvertiere den Rückgabewert in eine Ganzzahl
    student_id = last_student_id + 1
    student = Student(student_id, json_data.get("name"), json_data.get("level"))
    student_dict = student.to_dict()
    mongo.students_collection.insert_one(student_dict)
    return student_dict