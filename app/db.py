import pymongo


class MongoDB:
    def __init__(self):
        self.client = None

    def init_from_app(self, app):
        """
        Set up a mongoclient using app.config
        """
        mongo_uri = app.config["MONGO_URI"]
        self.client = self._get_mongo_client(mongo_uri)
        self.students_collection = self.client["school_db"]["students"]

    def _get_mongo_client(self, mongo_uri):
        """
        This function will be mocked in testing and modified
        to return a mongomock.MongoClient instead.
        """
        return pymongo.MongoClient(mongo_uri)

 

mongo = MongoDB()