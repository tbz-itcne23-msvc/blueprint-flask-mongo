from apiflask import Schema
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf

import random

####
## Schemas for OpenAPI and validation
####
class StudentIn(Schema):
    name = String(required=True, validate=Length(0, 32))
    level = String(required=True, validate=OneOf(['HF', 'PE', 'AP', 'ICT']))

class StudentOut(Schema):
    student_id = Integer()
    name = String()
    level = String()

class Student:
    def __init__(self, student_id, name, level):
        self.student_id = student_id
        self.name = name
        self.level = level
    
    def to_dict(self):
        student_dict = {
            "student_id" : self.student_id,
            "name" : self.name,
            "level" : self.level
        }
        return student_dict