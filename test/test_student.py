
def test_get_students(app):
    with app.test_client() as client:
        response = client.get("/students/")
        assert response.status_code == 200
        assert b"name" in response.data

def test_get_student(app):
    with app.test_client() as client:
        response = client.get("/students/2")
        assert response.status_code == 200
        assert response.json["name"] == "Sam Sung"

def test_create_student(app):
    with app.test_client() as client:
        response = client.post("/students/", json={
            'name': 'Nina Hagen', 'level': 'AP'
        })
        assert response.status_code == 201